import numpy as np 
import matplotlib.pyplot as plt

def estimate_b0_b1(x, y):
    n = np.size(x)
    #obtengo los promedios // Average of X and Y
    m_x, m_y = np.mean(x), np.mean(y)
    #sumatoria de XY y XX // Averages sum
    sumatoria_xy = np.sum((x-m_x)*(y-m_y))
    sumatoria_xx = np.sum(x*(x-m_x))
    #coeficientes de regresion // Regression coefficient
    b_1 = sumatoria_xy / sumatoria_xx
    b_0 = m_y - b_1 * m_x 

    return b_0, b_1

def plot_regression(x, y, b):
    plt.scatter(x, y, color='g', marker='o', s=30)

    y_pred = b[0] + b[1]*x 
    plt.plot(x, y_pred, color='b')
    #etiquetas // labels
    plt.xlabel('x-indep.')
    plt.ylabel('y-depend.')
    
    plt.show()

def main():
    #Dando datos a las variables X y Y // Giving a Set to X and Y
    x = np.array([1,2,4,0])
    y = np.array([0.5,1,2,0])

    b = estimate_b0_b1(x,y)
    print(f'b0 es {b[0]} y b1 es {b[1]}')
    plot_regression(x, y, b)

if __name__ == "__main__":
    main()